(define (domain vampire)
    (:requirements :conditional-effects)
    (:predicates
        (light-on ?r)
        (slayer-is-alive)
        (slayer-is-in ?r)
        (vampire-is-alive)
        (vampire-is-in ?r)
        (fighting)
        ;
        ; static predicates
        (NEXT-ROOM ?r ?rn)
        (CONTAINS-GARLIC ?r)
    )

    (:action toggle-light
        :parameters (?anti-clockwise-neighbor ?room ?clockwise-neighbor)
        :precondition (and
            (NEXT-ROOM ?anti-clockwise-neighbor ?room)
            (NEXT-ROOM ?room ?clockwise-neighbor)
            (not (fighting))
        )
        :effect(and 
			(when

				;; If lights are on

				(light-on ?room)
                (and

					;; Turn them off

					(not (light-on ?room))
					(when

						;;;;;;;;; SLAYER MOVES ;;;;;;;;;
						;; if the slayer's room is switched off, she moves to a neighboring room

						(slayer-is-in ?room)
						(and
							(when
								(light-on ?anti-clockwise-neighbor)
								(and
									(when

										;; If one of the rooms has lights on (in this case, the anti-clockwise one), choose this one

										(not (light-on ?clockwise-neighbor))
										(and
											(not (slayer-is-in ?room))
											(slayer-is-in ?anti-clockwise-neighbor)
											(when 
												(vampire-is-in ?anti-clockwise-neighbor)
												(fighting)
											)
										)
									)
									(when 
										
										;; If both rooms are bright, choose clockwise

										(light-on ?clockwise-neighbor)
										(and
											(not (slayer-is-in ?room))
											(slayer-is-in ?clockwise-neighbor)
											(when 
												(vampire-is-in ?clockwise-neighbor)
												(fighting)
											)
										)
									)
								)
							)
							(when
								(not (light-on ?anti-clockwise-neighbor))
								(and
									(when

										;; If one of the rooms has lights on (in this case, the clockwise one), choose this one

										(light-on ?clockwise-neighbor)
										(and
											(not (slayer-is-in ?room))
											(slayer-is-in ?clockwise-neighbor)
											(when 
												(vampire-is-in ?clockwise-neighbor)
												(fighting)
											)
										)
									)
									(when 

										;; If both rooms are dark, choose anti-clockwise

										(not (light-on ?clockwise-neighbor))
										(and
											(not (slayer-is-in ?room))
											(slayer-is-in ?anti-clockwise-neighbor)
											(when 
												(vampire-is-in ?anti-clockwise-neighbor)
												(fighting)
											)
										)
									)
								)
							)
						)						
					)
				)
			)

			;;;;;;;;; VAMPIRE MOVES ;;;;;;;;;
            ;; if the vampire's room light is switched on, the vampire moves to a neighboring room	

			(when

				;; If lights are off

				(not (light-on ?room))
                (and

					;; Turn them on

					(light-on ?room)
					(when
						(vampire-is-in ?room)
						(and
							(when
								(light-on ?anti-clockwise-neighbor)
								(and
									(when

										;; If one of the rooms has lights off (in this case, the clockwise one), choose this one

										(not (light-on ?clockwise-neighbor))
										(and
											(not (vampire-is-in ?room))
											(vampire-is-in ?clockwise-neighbor)
											(when 
												(slayer-is-in ?clockwise-neighbor)
												(fighting)
											)
										)
									)
									(when 

										;; If both rooms are bright, choose clockwise

										(light-on ?clockwise-neighbor)
										(and
											(not (vampire-is-in ?room))
											(vampire-is-in ?clockwise-neighbor)
											(when 
												(slayer-is-in ?clockwise-neighbor)
												(fighting)
											)
										)
									)
								)
							)
							(when
								(not (light-on ?anti-clockwise-neighbor))
								(and
									(when

										;; If one of the rooms has lights off (in this case, the anti-clockwise one), choose this one

										(light-on ?clockwise-neighbor)
										(and
											(not (vampire-is-in ?room))
											(vampire-is-in ?anti-clockwise-neighbor)
											(when 
												(slayer-is-in ?anti-clockwise-neighbor)
												(fighting)
											)
										)
									)
									(when 

										;; If both rooms are dark, choose anti-clockwise
				
										(not (light-on ?clockwise-neighbor))
										(and
											(not (vampire-is-in ?room))
											(vampire-is-in ?anti-clockwise-neighbor)
											(when 
												(slayer-is-in ?anti-clockwise-neighbor)
												(fighting)
											)
										)
									)
								)
							)
						)						
					)
				)
			)
		)
	)



    (:action watch-fight
        :parameters (?room)
        :precondition (and
            (slayer-is-in ?room)
            (slayer-is-alive)
            (vampire-is-in ?room)
            (vampire-is-alive)
            (fighting)
        )
        :effect (and
			;; if lights are on or there is garlic in the room, the slayer wins the fight
			(when
				(or 
					(light-on ?room )
					(CONTAINS-GARLIC ?room)
				)
				(and
					(not (vampire-is-alive))
					(not (fighting))
				)
			)
			;; if lights are switched off and there is no garlic in the room, the vampire wins the fight
			(when
				(and 
					(not (light-on ?room ))
					(not (CONTAINS-GARLIC ?room))
				)
				(and
					(not (slayer-is-alive))
					(not (fighting))
				)
			)
        )
    )
)
