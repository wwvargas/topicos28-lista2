#!/usr/bin/env python

from collections import defaultdict, deque
import sys

from pddl_types import *
from parser import parse_domain_file, parse_task_file


def regress_formula_through_strips_action(atoms, action):
    """
    'atoms' is a frozenset of strings (predicates) representing a
    conjunction C. The method returns another frozenset of strings
    representing the conjunction for the regression of C through the
    STRIPS action 'strips_action'.
    """
    regressions = []

    for a in action:
        # First Step:  Choose an operator o that deletes no ai
        if set(atoms).isdisjoint(a.delete_effects):
            # Optimization: only consider operators adding at least one ai
            if any(add in atoms for add in a.add_effects):
                # Second step: Remove any atoms added by o from subgoal
                regression = frozenset([
                    atom
                    for atom in atoms if (atom not in a.add_effects)
                ])
                # Third step: Conjoin pre(o)
                regression = regression.union(a.preconditions)
                regressions.append((a, regression))

    return regressions


def is_init(atoms, init):
    # Check if atoms contains init
    return set(atoms).issubset(init)


def regression_breadth_first_search(strips_task):
    """
    Performs a breadth-first regression search through the given task.
    returns a triple e, g, p, where e is the number of expanded states,
    g is the number of generated states, and p is the discovered plan
    (or None if the task is unsolvable).
    """

    # The queue contains tuples (path, formula) where formula describes
    # a set of states that can reach the goal with the actions in path.
    # Initially, the first element represents the set of goal states,
    # which can reach the goal with an empty plan.
    queue = deque([([], strips_task.goal)])

    # The closed list contains all formulas we have expanded so far.
    # You don't have to check for subset (formulas that imply a
    # previously expanded formula).
    closed = set()

    expanded = 0
    generated = 1

    # Following slide
    # "BFS-Graph (Breadth-First Search with Duplicate Elim.)" - Class 3
    if is_init(strips_task.goal, strips_task.init):
        return expanded, generated, None

    while queue:
        n = queue.popleft()
        expanded+=1
        # gets predecessors following rules at slide
        # "Regression for STRIPS Planning Tasks" - Class 12
        for regression in regress_formula_through_strips_action(n[1],strips_task.actions):
            o = regression[0]
            r = regression[1]

            # duple to append to queue, that is: (<updated path through regression to formula>, <formula>)
			# adding current operator to the beggining once this is a regression so, when one starts on initial
			# this operator will be applied before all the others already visited on regression to this formula
            n1 = ([o.name] + n[0], r)

			# if r is the initial state then the regression is over and a plan was found
            if is_init(r, strips_task.init):
                generated+=1 
                return expanded, generated, n1[0] # n1[0] is the plan from initial state to goal state found by regression
            if r not in closed:
				# add r to closed set once r represents the formula
                closed.add(r)
                queue.append(n1)
                generated+=1

    return expanded, generated, None

if __name__ == "__main__":
    if len(sys.argv) != 3:
        print "expected domain and task filename as the only parameters."
        sys.exit(1)
    domain_filename = sys.argv[1]
    domain = parse_domain_file(domain_filename)
    task_filename = sys.argv[2]
    task = parse_task_file(task_filename)
    strips_task = StripsTask(domain, task)
    expanded, generated, plan = regression_breadth_first_search(strips_task)

    print "Expanded %d states" % expanded
    print "Generated %d states" % generated
    if plan:
        print "Plan found:"
        print "\n".join(plan)
    else:
        print "Completely explored search space. No plan found."
